# /bin/bash

# ====================Firewall=======================================
ufw enable
ufw allow 80

# ======================Update system================================

apt update -y

# ======================Install nginx================================
apt install nginx -y
cp /vagrant/light/default.conf /etc/nginx/conf.d/default.conf
rm /etc/nginx/nginx.conf
cp /vagrant/light/nginx.conf /etc/nginx/nginx.conf
systemctl restart nginx

# ======================Install node=================================
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
apt-get install -y nodejs

# =====================Install app===================================
mkdir -p /usr/src/app
cd /usr/src/app

git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
cd 2048-game

npm install
npm run start &
sleep 5
